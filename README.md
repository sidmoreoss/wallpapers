# Wallpapers (Deprecated)

**Important:** This project is deprecated in favor of our new project, [uwal](https://gitlab.com/sidmoreoss/uwal). Please consider migrating to the new repository for the latest features and updates.

## Description
This was my wallpaper collection project, but it is no longer actively maintained. We have migrated to a new and improved project called [uwal](https://gitlab.com/sidmoreoss/uwal), which offers a more feature-rich experience.

## Migration to uwal
To continue enjoying wallpapers and receive updates, we recommend migrating to [uwal](https://gitlab.com/sidmoreoss/uwal). You can follow these steps:

1. Visit the [uwal repository](https://gitlab.com/sidmoreoss/uwal).
2. Follow the installation and usage instructions provided in the uwal README.
3. If you have any data or settings from this deprecated project that you'd like to transfer, please refer to the uwal documentation for migration guides.

## Contributing
This deprecated project is no longer actively maintained. If you wish to contribute or access updated wallpapers, please visit [uwal](https://gitlab.com/sidmoreoss/uwal) and get involved there.

## Authors and acknowledgment
We would like to thank Unsplash and Wallpapers wide for their contributions to this project.

## License
MIT

## Project status
Deprecated. Please consider migrating to [uwal](https://gitlab.com/sidmoreoss/uwal) for an improved wallpaper experience.
